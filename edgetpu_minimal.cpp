// Example to run a model using one Edge TPU.
// It depends only on tflite and edgetpu.h

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <chrono>

#include "edgetpu.h"
#include <tensorflow/lite/builtin_op_data.h>
#include <tensorflow/lite/kernels/register.h>
#include <tensorflow/lite/kernels/internal/tensor_ctypes.h>
#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/model.h"

template<typename U, typename T>
class Differ {
  public:
    Differ() 
      : cnt(0),  avg(0),
        high(0), low(std::numeric_limits<U>::max()),
        begin_(), end_(),
        diff_(0), diff_sum_(0) {
    }
    ~Differ() {}

    inline void begin() { 
      begin_ = std::chrono::steady_clock::now();
    }

    inline void end() { 
      using namespace std::chrono;

      end_ = steady_clock::now();

      duration<U,T> span = 
        duration_cast<duration<U,T>>(end_ - begin_);

      diff_ = span.count();
      diff_sum_ += diff_;

      high = (high < diff_) ? diff_ : high;
      low = (low > diff_) ? diff_ : low;

      cnt++;
      avg = diff_sum_ / cnt;
    }

  public:
    U cnt;
    U avg;
    U high;
    U low;

  private:
    std::chrono::steady_clock::time_point begin_;
    std::chrono::steady_clock::time_point end_;
    U diff_;
    U diff_sum_;
};

void resize(std::unique_ptr<tflite::Interpreter>& interpreter,
    uint8_t* out, const uint8_t* in,
    int image_height, int image_width, int image_channels, 
    int wanted_height, int wanted_width, int wanted_channels) {

  int number_of_pixels = image_height * image_width * image_channels;

  // fill input image
  // in[] are integers, cannot do memcpy() directly
  auto input = interpreter->typed_tensor<float>(0);
  for (int i = 0; i < number_of_pixels; i++) {
    input[i] = in[i];
  }

  // fill new_sizes
  interpreter->typed_tensor<int>(1)[0] = wanted_height;
  interpreter->typed_tensor<int>(1)[1] = wanted_width;

  interpreter->Invoke();

  auto output = interpreter->typed_tensor<float>(2);
  auto output_number_of_pixels = wanted_height * wanted_width * wanted_channels;

  for (int i = 0; i < output_number_of_pixels; i++) {
    out[i] = (uint8_t)output[i];
  }
}

std::vector<uint8_t> decode_bmp(const uint8_t* input, int row_size, int width,
                                int height, int channels, bool top_down) {
  std::vector<uint8_t> output(height * width * channels);
  for (int i = 0; i < height; i++) {
    int src_pos;
    int dst_pos;

    for (int j = 0; j < width; j++) {
      if (!top_down) {
        src_pos = ((height - 1 - i) * row_size) + j * channels;
      } else {
        src_pos = i * row_size + j * channels;
      }

      dst_pos = (i * width + j) * channels;

      switch (channels) {
        case 1:
          output[dst_pos] = input[src_pos];
          break;
        case 3:
          // BGR -> RGB
          output[dst_pos] = input[src_pos + 2];
          output[dst_pos + 1] = input[src_pos + 1];
          output[dst_pos + 2] = input[src_pos];
          break;
        case 4:
          // BGRA -> RGBA
          output[dst_pos] = input[src_pos + 2];
          output[dst_pos + 1] = input[src_pos + 1];
          output[dst_pos + 2] = input[src_pos];
          output[dst_pos + 3] = input[src_pos + 3];
          break;
        default:
          std::cerr << "Unexpected number of channels: " << channels
                    << std::endl;
          std::abort();
          break;
      }
    }
  }
  return output;
}

std::vector<uint8_t> read_bmp(const std::string& input_bmp_name, int* width,
                              int* height, int* channels) {
  int begin, end;

  std::ifstream file(input_bmp_name, std::ios::in | std::ios::binary);
  if (!file) {
    std::cerr << "input file " << input_bmp_name << " not found\n";
    std::abort();
  }

  begin = file.tellg();
  file.seekg(0, std::ios::end);
  end = file.tellg();
  size_t len = end - begin;

  std::vector<uint8_t> img_bytes(len);
  file.seekg(0, std::ios::beg);
  file.read(reinterpret_cast<char*>(img_bytes.data()), len);
  const int32_t header_size =
      *(reinterpret_cast<const int32_t*>(img_bytes.data() + 10));
  *width = *(reinterpret_cast<const int32_t*>(img_bytes.data() + 18));
  *height = *(reinterpret_cast<const int32_t*>(img_bytes.data() + 22));
  const int32_t bpp =
      *(reinterpret_cast<const int32_t*>(img_bytes.data() + 28));
  *channels = bpp / 8;

  // there may be padding bytes when the width is not a multiple of 4 bytes
  // 8 * channels == bits per pixel
  const int row_size = (8 * *channels * *width + 31) / 32 * 4;

  // if height is negative, data layout is top down
  // otherwise, it's bottom up
  bool top_down = (*height < 0);

  // Decode image, allocating tensor once the image size is known
  const uint8_t* bmp_pixels = &img_bytes[header_size];
  return decode_bmp(bmp_pixels, row_size, *width, abs(*height), *channels,
                    top_down);
}

int main(int argc, char* argv[]) {
  if (argc != 1 && argc != 3) {
    std::cout << " edgetpu_minimal <edgetpu model path> <input image path>" << std::endl;
    return 1;
  }

  const auto& available_tpus =
      edgetpu::EdgeTpuManager::GetSingleton()->EnumerateEdgeTpu();
  if (available_tpus.size() < 1) {
    std::cerr << "This example requires one Edge TPUs to run." << std::endl;
    return 0;
  }

  const std::string model_path =
      argc == 3 ? argv[1]
                : "./models/mobilenet_v1_1.0_224_quant_edgetpu.tflite";
  const std::string image_path =
      argc == 3 ? argv[2] : "./images/cat.bmp";

  // read the image file.
  std::cout << "read image" << std::endl;
  Differ<uint32_t,std::micro> differ_read;
  differ_read.begin();
  int width, height, channels;
  const std::vector<uint8_t>& image =
      read_bmp(image_path, &width, &height, &channels);
  differ_read.end();

  // read model.
  std::cout << "load model" << std::endl;
  Differ<uint32_t,std::micro> differ_load;
  differ_load.begin();
  std::unique_ptr<tflite::FlatBufferModel> model =
      tflite::FlatBufferModel::BuildFromFile(model_path.c_str());
  if (model == nullptr) {
    std::cerr << "Fail to build FlatBufferModel from file: " << model_path
              << std::endl;
    std::abort();
  }
  differ_load.end();

  // build interpreter.
  std::cout << "build interpreters" << std::endl;
  Differ<uint32_t,std::micro> differ_build;
  differ_build.begin();
  std::shared_ptr<edgetpu::EdgeTpuContext> edgetpu_context =
      edgetpu::EdgeTpuManager::GetSingleton()->OpenDevice();
  tflite::ops::builtin::BuiltinOpResolver resolver;
  resolver.AddCustom(edgetpu::kCustomOp, edgetpu::RegisterCustomOp());
  std::unique_ptr<tflite::Interpreter> interpreter;
  if (tflite::InterpreterBuilder(*model, resolver)(&interpreter) != kTfLiteOk) {
    std::cerr << "Failed to build interpreter." << std::endl;
  }
  interpreter->SetExternalContext(kTfLiteEdgeTpuContext, edgetpu_context.get());
  interpreter->SetNumThreads(1);
  if (interpreter->AllocateTensors() != kTfLiteOk) {
    std::cerr << "Failed to allocate tensors." << std::endl;
  }
  int input = interpreter->inputs()[0];
  const std::vector<int> inputs = interpreter->inputs();
  const std::vector<int> outputs = interpreter->outputs();
  TfLiteIntArray* dims = interpreter->tensor(input)->dims;
  int wanted_height = dims->data[1];
  int wanted_width = dims->data[2];
  int wanted_channels = dims->data[3];

  // build resize interpreter
  std::unique_ptr<tflite::Interpreter> resize_interpreter;
  resize_interpreter = std::make_unique<tflite::Interpreter>();
  int base_index = 0;
  resize_interpreter->AddTensors(2, &base_index); // two inputs: input and new_size
  resize_interpreter->AddTensors(1, &base_index); // one output
  resize_interpreter->SetInputs({0, 1});
  resize_interpreter->SetOutputs({2});
  TfLiteQuantizationParams quant;
  resize_interpreter->SetTensorParametersReadWrite(
      0, kTfLiteFloat32, "input",
      {1, height, width, channels}, 
      quant);
  resize_interpreter->SetTensorParametersReadWrite(
      1, kTfLiteInt32, "new_size", 
      {2}, 
      quant);
  resize_interpreter->SetTensorParametersReadWrite(
      2, kTfLiteFloat32, "output",
      {1, wanted_height, wanted_width, wanted_channels}, 
      quant);
  tflite::ops::builtin::BuiltinOpResolver resize_resolver;
  const TfLiteRegistration* resize_op =
      resize_resolver.FindOp(tflite::BuiltinOperator_RESIZE_BILINEAR, 1);
  auto* params = reinterpret_cast<TfLiteResizeBilinearParams*>(
      malloc(sizeof(TfLiteResizeBilinearParams)));
  params->align_corners = false;
  resize_interpreter->AddNodeWithParameters(
      {0, 1}, {2}, nullptr, 0, params, resize_op, nullptr);
  resize_interpreter->SetExternalContext(kTfLiteEdgeTpuContext, edgetpu_context.get());
  resize_interpreter->SetNumThreads(1);
  if (resize_interpreter->AllocateTensors() != kTfLiteOk) {
    std::cerr << "Failed to allocate resize tensors." << std::endl;
  }
  differ_build.end();

  // resize & invoke
  std::cout << "resize and invoke" << std::endl;
  Differ<uint32_t,std::micro> differ_resize;
  Differ<uint32_t,std::micro> differ_invoke;
  for (int i = 0; i < 1000; i++) {
    std::cout << "." << std::flush;

    // resize
    differ_resize.begin();
    if (interpreter->tensor(input)->type == kTfLiteUInt8) {
      resize(resize_interpreter,
          interpreter->typed_tensor<uint8_t>(input),
          image.data(), height, width, channels,
          wanted_height, wanted_width, wanted_channels);
    } else {
      std::cout << "unrecognized output" << std::endl;
      std::abort();
    }
    differ_resize.end();

    // invoke
    differ_invoke.begin();
    interpreter->Invoke();
    differ_invoke.end();
  }
  std::cout << std::endl;

  // results
  Differ<uint32_t,std::micro> differ_results;
  differ_results.begin();
  std::vector<float> result;
  const auto& output_indices = interpreter->outputs();
  const int num_outputs = output_indices.size();
  int out_idx = 0;
  for (int i = 0; i < num_outputs; ++i) {
    const auto* out_tensor = interpreter->tensor(output_indices[i]);
    assert(out_tensor != nullptr);
    if (out_tensor->type == kTfLiteUInt8) {
      const int num_values = out_tensor->bytes;
      result.resize(out_idx + num_values);
      const uint8_t* output = interpreter->typed_output_tensor<uint8_t>(i);
      for (int j = 0; j < num_values; ++j) {
        result[out_idx++] = (output[j] - out_tensor->params.zero_point) *
                                 out_tensor->params.scale;
      }
    } else if (out_tensor->type == kTfLiteFloat32) {
      const int num_values = out_tensor->bytes / sizeof(float);
      result.resize(out_idx + num_values);
      const float* output = interpreter->typed_output_tensor<float>(i);
      for (int j = 0; j < num_values; ++j) {
        result[out_idx++] = output[j];
      }
    } else {
      std::cerr << "Tensor " << out_tensor->name
                << " has unsupported output type: " << out_tensor->type
                << std::endl;
    }
  }
  differ_results.end();

  std::cout << "results" << std::endl;
  std::cout << std::endl;
  auto it = std::max_element(result.begin(), result.end());
  std::cout << "[Image analysis] max value index: "
            << std::distance(result.begin(), it) << " value: " << *it
            << std::endl;

  printf("\nMinimal Results...\n");
  printf("            read image (us): high:%u avg:%u low:%u cnt:%u\n", 
    differ_read.high, differ_read.avg, 
    differ_read.low,  differ_read.cnt);
  printf("            load model (us): high:%u avg:%u low:%u cnt:%u\n", 
    differ_load.high, differ_load.avg, 
    differ_load.low,  differ_load.cnt);
  printf("    build interpreters (us): high:%u avg:%u low:%u cnt:%u\n", 
    differ_build.high, differ_build.avg, 
    differ_build.low,  differ_build.cnt);
  printf("          resize image (us): high:%u avg:%u low:%u cnt:%u\n", 
    differ_resize.high, differ_resize.avg, 
    differ_resize.low,  differ_resize.cnt);
  printf("          invoke image (us): high:%u avg:%u low:%u cnt:%u\n", 
    differ_invoke.high, differ_invoke.avg, 
    differ_invoke.low,  differ_invoke.cnt);
  printf("               results (us): high:%u avg:%u low:%u cnt:%u\n", 
    differ_results.high, differ_results.avg, 
    differ_results.low,  differ_results.cnt);


  return 0;
}
