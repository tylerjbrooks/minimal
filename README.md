## Edge TPU Minimal Example

This application (edgetpu_minimal) is a minimal example of using the 
[Edge TPU](https://coral.ai/products/accelerator/) on a 
i[Raspberry Pi 3 B+](https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/).  The
program will read the bitmap and then resize & interpret it 1000 times.  It then prints out
the result.  This application is written in C++. 

Many thanks to the [Google Coral](https://coral.ai/) project for providing the TPU accelerator.

Many thanks to the [Tensorflow](https://www.tensorflow.org) project for doing the object detection heavy lifting.

### Installation

Edgetpu_minimal  is cross compiled on a desktop and copied to the target rpi3b+.  As such, the build 
process will require some environment variables so it can find the various pieces of 
Detector.  The following is how my environment is setup:
```
# project directory
export RASPBIAN=~/your/workspace/raspbian
# project cross-compiler
export RASPBIANCROSS=arm-linux-gnueabihf-
# TensorFlow location
export TFLOWSDK=$RASPBIAN/tensorflow
# Edgetpu location
export EDGETPUSDK=$RASPBIAN/edgetpu
```

Get Tensorflow, Edgetpu and Edgetpu_minimal like this:
```
cd your/workspace/raspbian
git clone https://gitlab.com:tylerjbrooks/tensorflow.git
cd tensorflow
git checkout d855adf  #tensorflow commit d855adf
cd ..
git clone https://github.com/google-coral/edgetpu
git clone https://gitlab.com:tylerjbrooks/edgetpu_minimal.git

```
note:  the Edgetpu sdk currently only works with the 'd855adf' commit of tensorflow.

Get the Raspbian tool chain like this:
```
sudo apt-get install build-essential
sudo apt-get install g++-arm-linux-gnueabihf
```

At this point, you should have all the software you need to build Detector.

### Build Notes

Start by building Tensorflow Lite:
```
cd your/workspace/raspbian
cd tensorflow/tensorflow/lite/tools/make
./download_dependencies.sh
./build_rpi_lib.sh
```

Build Edgetpu_minimal:
```
cd your/workspace/raspbian
cd edgetpu_minimal
make
```

### Usage
```
edgetpu_minimal <edgetpu model path> <input image path>
  default model: ./models/mobilenet_v1_1.0_224_quant_edgetpu.tflite
  default image: ./images/cat.bmp
```

The default output looks like this:
```
t:0,l:3,b:479,r:632, scor:0.910156, class:14
./edgetpu_minimal 
read image
load model
build interpreters
resize and invoke
............ <print 1000 dots> ..................
results

[Image analysis] max value index: 286 value: 0.785156

Minimal Results...
            read image (us): high:37057 avg:37057 low:37057 cnt:1
            load model (us): high:195699 avg:195699 low:195699 cnt:1
    build interpreters (us): high:3242012 avg:3242012 low:3242012 cnt:1
          resize image (us): high:49468 avg:19851 low:19727 cnt:1000
          invoke image (us): high:138462 avg:8785 low:8387 cnt:1000
               results (us): high:22 avg:22 low:22 cnt:1

```
Notice that the resize (avg: 19851)  and invoke (avg: 8785) times are very 
low.  Typically they average around 50ms and 200ms respectively

### Discussion
I make this program to measure the TPU's resize and inference times.

### Notes

### To Do

