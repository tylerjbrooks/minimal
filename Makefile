# environment variables:
#   RASPBIAN is the location of your raspbian projects
#   RASPBIANCROSS is the location of the raspbian compiler tools
#   EDGETPUSDK is the location of the edgetpu sdk
#   TFLOWLITESDK is the location of your 'tensorflow-lite' sdk

CXX = $(RASPBIANCROSS)g++

SRC = edgetpu_minimal.cpp
OBJ = $(SRC:.cpp=.o)
EXE = edgetpu_minimal

CFLAGS = -fPIC -Wall -g -ftree-vectorize -pipe -O3 -std=c++17 -march=armv7-a -mfpu=neon-vfpv4 -Wno-psabi

LDFLAGS = \
	-L. \
	-L$(TFLOWSDK)/tensorflow/lite/tools/make/gen/rpi_armv7l/lib \
	-L$(EDGETPUSDK)/libedgetpu/direct/armv7a

LIBS = -ltensorflow-lite 
LIBS += -l:libedgetpu.so.1.0 
LIBS += -lpthread -ldl -lrt -lm

#add these if cross compiling
# this is weird but I can't seem to 'apt install libuse-1.0-dev' so I have 
# copied them into the home directory from the rpi
LDFLAGS += -L.
LIBS += -l:libc.so.6 -l:libudev.so.1 -l:libusb-1.0.so.0

INCLUDES = \
	-I. \
	-I$(TFLOWSDK) \
	-I$(TFLOWSDK)/tensorflow/lite/tools/make/downloads/flatbuffers/include \
	-I$(TFLOWSDK)/tensorflow/lite/tools/make/downloads/absl \
	-I$(EDGETPUSDK) \
	-I$(EDGETPUSDK)/libedgetpu 


$(EXE): $(OBJ)
	$(CXX) $(LDFLAGS) $(OBJ) $(LIBS) -o $@

.cpp.o:
	$(CXX) $(CFLAGS) $(INCLUDES) -c $< -o $@

.PHONY: clean
clean:
	rm -f $(EXE) $(OBJ)

